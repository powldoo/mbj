<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>企業が作成されました</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>
    <p>Hi <b>{{ $user['name'] }}</b></p>

    <p>You have successfully updated your user information. Here are the details:</p><br/>

    Account Email: {{ $user['email'] }}<br/>
    Account Password: <b>{{ $user['password'] }}</b><br/><br/>

    <p>Note: If it is not you, please call this hotline number for assistance: (032) 123-4567</p>

    <p>Thanks and Regards,<br/>
    MBJ Team</p>
</body>
</html>

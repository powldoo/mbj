<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>企業が作成されました</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>
    <p>Hi <b>{{ $user['name'] }}</b></p>

    <p>Here is your account details for <a href="http://mbj.dev">MBJ Corporation - Solid Waste Management System</a>
    under your Company/Organization <b>{{ $user['company_name'] }}</b></p> <br/>

    Account Email: {{ $user['email'] }}<br/>
    Temporary Account Password: <b>{{ $user['password'] }}</b><br/><br/>

    <p>Note: Please do change your email address as soon as you received this email. If you need assistance, you may call this hotline number: (032) 123-4567</p>

    <p>Thanks and Regards,<br/>
    MBJ Team</p>
</body>
</html>

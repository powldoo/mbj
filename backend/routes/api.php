<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/** Super Admin/Admin Controller */
Route::put('users/{user}/state/{state}', 'UserController@updateState');
Route::resource('users', 'UserController');

/** Client Controller */
Route::post('clients/store', 'ClientController@storeClient');
Route::put('clients/activate/{client}', 'ClientController@clientActivation');
Route::resource('clients', 'ClientController');

/** Company */
Route::resource('company', 'CompanyController');

/** Profile */
Route::get('profile/me', 'ProfileController@show');
Route::get('signout', 'ProfileController@signout');

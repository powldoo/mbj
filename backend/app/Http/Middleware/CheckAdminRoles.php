<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAdminRoles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $roles = collect([1, 2]);

        if (!$roles->contains(Auth::user()->role_id)) {
            return response()->json(['error' => '401 Unauthorized']);
        }

        return $next($request);
    }
}

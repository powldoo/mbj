<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = User::find(Auth::user()->id);
            return $next($request);
        });

        $this->middleware('auth:api')->only(['show', 'signout']);
    }

    public function show()
    {
        return new UserResource($this->user);
    }

    public function signout()
    {
        $token = Auth::user()->token();
        $token->revoke();

        return response()->json(['message' => 'Successfully Signed out!']);
    }
}

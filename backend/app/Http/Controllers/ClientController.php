<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\StoreClientRequest;
use App\Http\Requests\UpdateClientRequest;
use App\Http\Resources\UserResource;
use App\Repositories\ClientRepository;

class ClientController extends Controller
{
    public function __construct(ClientRepository $repository)
    {
        $this->repo = $repository;

        $this->middleware(['auth:api', 'admin.roles'])->only(['index']);
        $this->middleware(['auth:api', 'sadmin.role'])->only(['storeClient', 'clientActivation']);

        $this->middleware('auth:api')->only(['show', 'update']);
        $this->middleware('client')->only(['store']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return UserResource::collection(User::where('role_id', 3)->get());
    }

    /**
     * Store a newly created resource in storage.
     * Store Client via Website
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreClientRequest $request)
    {
        return new UserResource($this->repo->create($request->all(), false));
    }

    /**
     * Store Client via Admin Page
     */
    public function storeClient(StoreClientRequest $request)
    {
        return new UserResource($this->repo->create($request->all(), true));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $client)
    {
        return new UserResource($client);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateClientRequest $request, User $client)
    {
        return new UserResource($this->repo->update($client, $request->all()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Activate Client Account
     */
    public function clientActivation(User $client)
    {
        return $this->repo->activate($client);
    }
}

<?php

namespace App\Http\Controllers;

use App\CommonState;
use Illuminate\Http\Request;
use App\User;
use App\Http\Resources\UserResource;
use App\Http\Requests\StoreClientRequest;
use App\Http\Requests\StoreUserAdminRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct(UserRepository $repository)
    {
        $this->repo = $repository;

        $this->middleware(['auth:api', 'sadmin.role'])->only(['index', 'store', 'updateState']);
        $this->middleware(['auth:api', 'admin.roles'])->only(['show', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return UserResource::collection(User::where('role_id', '!=', 3)->where('id', '!=', Auth::id())->get());
    }

    /**
     * Store a newly created resource in storage.
     * Store new admin user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserAdminRequest $request)
    {
        return new UserResource($this->repo->create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        return new UserResource($this->repo->update($user, $request->all()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /** Update User Admin State */
    public function updateState(User $user, CommonState $state)
    {
        User::where('id', $user->id)->update(['state_id' => $state->id]);

        return new UserResource(User::find($user->id));
    }
}

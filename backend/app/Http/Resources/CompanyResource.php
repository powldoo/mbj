<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'address' => $this->address,
            'barangay' => $this->barangay,
            'city' => $this->city,
            'code' => $this->code,
            'contact_number_1' => $this->contact_number_1,
            'contact_number_2' => $this->contact_number_2,
            'email' => $this->email,
            'amount_per_kilo' => $this->amount_per_kilo,
            'minimum_kilos' => $this->minimum_kilos,
        ];
    }
}

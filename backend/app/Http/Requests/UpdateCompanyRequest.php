<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'address' => 'required',
            'barangay' => 'required',
            'city' => 'required',
            'code' => 'required',
            'contact_number_1' => 'required|numeric',
            'contact_number_2' => 'nullable|numeric',
            'email' => 'nullable|email|unique:companies,email',
        ];
    }
}

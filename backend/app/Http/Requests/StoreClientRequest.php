<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'company_name' => 'required',
            'address' => 'required',
            'barangay' => 'required',
            'city' => 'required',
            'code' => 'required',
            'contact_number_1' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'email.unique' => 'You have sent an inquiry using this email. Please check your mailbox.'
        ];
    }
}

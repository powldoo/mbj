<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\AuthenticationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($request->expectsJson()) {
            $code = 500;
            $response = [
                'error' => $exception->getMessage(),
            ];

            // handle http exception status code
            if ($exception instanceof HttpException) {
                $code = $exception->getStatusCode();
            }

            // handle model not found exceptions
            // when using route model binding or findOrFail()
            if ($exception instanceof ModelNotFoundException) {
                $model = str_replace('App\\', '', $exception->getModel());
                $response['error'] = $model . ' does not exists.';
            }

            // handle not found exception
            // and return custom error message
            if ($exception instanceof NotFoundHttpException) {
                $response['error'] = '404 Not Found.';
            }

            // handle laravel validation errors
            if ($exception instanceof ValidationException) {
                $response['error'] = $exception->validator->errors();
            }

            // handle laravel authentication exception
            if ($exception instanceof AuthenticationException) {
                $code = 401;
                $response['error'] = 'Unauthenticated.';
            }

            // json response
            return response()->json($response, $code);
        }

        return parent::render($request, $exception);
    }
}

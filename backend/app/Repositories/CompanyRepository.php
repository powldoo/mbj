<?php

namespace App\Repositories;

use App\Company;

class CompanyRepository
{
    /**
     * Update Company Details
     */
    public function update($data, $company)
    {
        Company::where('id', $company->id)->update($data);

        return Company::find($company);
    }
}

<?php

namespace App\Repositories;

use App\User;
use App\Company;
use App\Mail\ClientActivation;
use App\Mail\UserUpdateDetails;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Mail;

class ClientRepository
{
    /**
     * Contact Form - Client Details
     */
    public function create($data, $auth)
    {
        $array = [
            'name' => $data['company_name'],
            'address' => $data['address'],
            'barangay' => $data['barangay'],
            'city' => $data['city'],
            'code' => $data['code'],
            'contact_number_1' => $data['contact_number_1'],
        ];

        $company = Company::create($array);
        $data['company_id'] = $company->id;

        if ($auth === true) {
            $data['state_id'] = 1;
        } else {
            $data['state_id'] = 2;
        }

        $data['role_id'] = 3;

        return User::create($data);
    }

    /**
     * Activate Client Account
     */
    public function activate($data)
    {
        $user = [
            'email' => $data->email,
            'password' => Str::random(12),
            'name' => $data->name,
            'company_name' => $data->company->name,
        ];

        User::where('role_id', 3)
            ->where('state_id', 2)
            ->where('id', $data->id)->update([
                'password' => Hash::make($user['password']),
                'state_id' => 1
            ]);

        Mail::to($data->email)->send(new ClientActivation($user));

        return response()->json(['message' => 'Client Account successfully activated.']);
    }

    /**
     * Update User Details
     */
    public function update($user, $data)
    {
        User::where('id', $user->id)
            ->where('role_id', 3)
            ->update([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
            ]);

        Mail::to($data['email'])->send(new UserUpdateDetails($data));

        return User::find($user->id);
    }
}

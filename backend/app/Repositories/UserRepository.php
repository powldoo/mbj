<?php

namespace App\Repositories;

use App\User;
use App\Company;
use App\Mail\UserUpdateDetails;
use Illuminate\Support\Facades\Hash;
use Mail;

class UserRepository
{
    /** Create User Admin */
    public function create($data)
    {
        $company = Company::where('name', 'MBJ Corporation')->first();
        $data['company_id'] = $company->id;
        $data['state_id'] = 1;
        $data['role_id'] = 2;

        return User::create($data);
    }

    /**
     * Update User Details
     */
    public function update($user, $data)
    {
        User::where('id', $user->id)->update([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        Mail::to($data['email'])->send(new UserUpdateDetails($data));

        return User::find($user->id);
    }
}

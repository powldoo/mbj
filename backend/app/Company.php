<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'address', 'barangay', 'city', 'code', 'contact_number_1', 'contact_number_2', 'email'
    ];
}

<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{
    public function run()
    {
        $roles = [
            'Super Administrator' => 'Super Administrator',
            'Administrator' => 'Administrator',
            'Client' => 'Client',
        ];

        foreach ($roles as $key => $value) {
            $role = new Role;
            $role->name = $key;
            $role->description = $value;
            $role->save();
        }
    }
}

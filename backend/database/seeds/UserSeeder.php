<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Super Administrator',
            'email' => 'sadmin@mbj.com',
            'password' => Hash::make('secret'),
            'role_id' => 1,
            'state_id' => 1,
            'company_id' => 1,
        ]);

        User::create([
            'name' => 'Administrator',
            'email' => 'admin@mbj.com',
            'password' => Hash::make('secret'),
            'role_id' => 2,
            'state_id' => 1,
            'company_id' => 1,
        ]);
    }
}

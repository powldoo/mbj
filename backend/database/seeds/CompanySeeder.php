<?php

use Illuminate\Database\Seeder;
use App\Company;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Company::create([
            'name' => 'MBJ Corporation',
            'address' => 'National Highway',
            'barangay' => 'Barangay Basak',
            'city' => 'Mandaue City',
            'code' => '6014',
            'contact_number_1' => '09996588741',
            'email' => 'mbj@test.com',
            'amount_per_kilo' => 4.50,
            'minimum_kilos' => 200,
        ]);
    }
}

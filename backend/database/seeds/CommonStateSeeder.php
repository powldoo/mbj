<?php

use Illuminate\Database\Seeder;
use App\CommonState;

class CommonStateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $commonStates = ['Active', 'Inactive'];

        foreach ($commonStates as $key) {
            $commonState = new CommonState;
            $commonState->name = $key;
            $commonState->save();
        }
    }
}

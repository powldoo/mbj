import * as types from './actionTypes'

export function fetchAdminUsers() {
    return {
        type: types.FETCH_ADMIN_USERS
    }
}

export function successAdminUsers(data) {
    return {
        type: types.SUCCESS_ADMIN_USERS,
        payload: data
    }
}

export function failAdminUsers(error) {
    return {
        type: types.FAIL_ADMIN_USERS,
        payload: error
    }
}
import * as types from './actionTypes';

const initialState = {
    data: {},
    failed: false,
    fetching: false,
    error: {}
}

function reducer(state = initialState, action) {
    const { type, payload } = action;

    switch (type) {
        case types.FETCH_ADMIN_USERS:
            return {
                ...state,
                fetching: true,
            }
        case types.SUCCESS_ADMIN_USERS:
            return {
                ...state,
                fetching: false,
                data: { ...payload }
            }
        case types.FAIL_ADMIN_USERS:
            return {
                ...state,
                fetching: false,
                failed: true,
                error: { payload }
            }
        default:
            return state;
    }
}

export default reducer;
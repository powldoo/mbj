import React, { useEffect, useState } from 'react'
import { Grid, Paper, Button, LinearProgress, TablePagination } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import TableContainer from '@material-ui/core/TableContainer'
import { useDispatch, useSelector } from 'react-redux';
import { adminList } from '../service'

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    title: {
        margin: theme.spacing(2)
    },
    paper: {
        padding: theme.spacing(2)
    },
    table: {
        minWidth: 650,
    },
    button: {
        margin: theme.spacing(0.5, 1),
        minWidth: 120
    },
    editButton: {
        margin: theme.spacing(0.5, 1),
        minWidth: 120,
        color: '#4caf50',
        border: '1px solid #a5d6a7'
    },
}))

export default function Users() {
    const classes = useStyles()
    const dispatch = useDispatch()
    const userAdminList = useSelector(state => state.adminList)
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(5);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = event => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    const handleOnChange = (event) => {
        console.log(event.target)
    }

    useEffect(() => {
        dispatch(adminList())
    }, [dispatch]);

    const arrUserList = Object.entries(userAdminList.data)

    return (
        <div className={classes.root}>
            <Grid container spacing={2}>
                <Grid item xs={12} sm={12}>
                    <Typography gutterBottom variant="h5" component="h2" className={classes.title}>
                        Admin Users
                    </Typography>
                    <TableContainer component={Paper} className={classes.paper}>
                        {userAdminList.fetching && (
                            <LinearProgress />
                        )}
                        {!userAdminList.fetching && (
                            <Table stickyHeader className={classes.table} aria-label="sticky table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Name</TableCell>
                                        <TableCell>Email</TableCell>
                                        <TableCell>Role</TableCell>
                                        <TableCell>Status</TableCell>
                                        <TableCell>Actions</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {arrUserList.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((value, key) => (
                                        <TableRow key={key}>
                                            <TableCell>{value[1].name}</TableCell>
                                            <TableCell>{value[1].email}</TableCell>
                                            <TableCell>{value[1].role.name}</TableCell>
                                            <TableCell>{value[1].state}</TableCell>
                                            <TableCell>
                                                <Button variant="outlined" className={classes.editButton} value={value[1].id}>
                                                    Edit
                                                </Button>
                                                {value[1].state === 'Active' && (
                                                    <Button variant="outlined" color="secondary" className={classes.button} value={value[1].id} onClick={handleOnChange} >
                                                        Deactivate
                                                    </Button>
                                                )}
                                                {value[1].state === 'Inactive' && (
                                                    <Button variant="outlined" color="primary" className={classes.button} value={value[1].id} onClick={handleOnChange}>
                                                        Activate
                                                    </Button>
                                                )}
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        )}
                        <TablePagination
                            rowsPerPageOptions={[5, 10, 15]}
                            component="div"
                            count={arrUserList.length}
                            rowsPerPage={rowsPerPage}
                            page={page}
                            onChangePage={handleChangePage}
                            onChangeRowsPerPage={handleChangeRowsPerPage}
                        />
                    </TableContainer>
                </Grid>
            </Grid>
        </div >
    )
}

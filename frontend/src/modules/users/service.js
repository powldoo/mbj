import Http from '../../utilities/Http'
import {
    fetchAdminUsers,
    successAdminUsers,
    failAdminUsers
} from './store/actionCreators'

export function adminList() {
    return function (dispatch) {
        dispatch(fetchAdminUsers())

        return Http.get('api/users')
            .then(response => {
                dispatch(successAdminUsers(response.data.data))
            })
            .catch(error => {
                dispatch(failAdminUsers(error))
            })
    }
}
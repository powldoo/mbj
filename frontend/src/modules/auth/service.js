import Http from '../../utilities/Http';
import { showLoader, hideLoader } from '../loader/store/actionCreators';
import {
    authRequestSignIn,
    authSignIn,
    authFailSignIn,
    authSignOut,
} from './store/actionCreators';

export function signInUser(credentials) {
    return function (dispatch) {
        /**
         * The app state is updated to inform
         * that the API call is starting.
         */
        dispatch(authRequestSignIn());

        dispatch(showLoader());

        const config = {
            grant_type: 'password',
            client_id: process.env.REACT_APP_CLIENT_ID,
            client_secret: process.env.REACT_APP_CLIENT_SECRET,
        };

        const data = { ...credentials, ...config }

        // actual api call
        return Http.post('oauth/token', data)
            .then(response => {
                dispatch(authSignIn(response.data));
            })
            .catch(error => {
                dispatch(authFailSignIn(error.response.data));
            })
            .finally(() => {
                dispatch(hideLoader());
            });
    };
}

export function signOutUser() {
    return function (dispatch) {
        dispatch(showLoader());

        return Http.get('api/signout')
            .then(() => {
                dispatch(authSignOut());
            })
            .catch(error => {
                console.log(error);
            })
            .finally(() => {
                dispatch(hideLoader());
            });
    };
}
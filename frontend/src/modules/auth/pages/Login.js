import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import validate from 'validate.js';
import Logo from '../../../assets/logo.jpg'
import {
    TextField,
    Container,
    Typography,
    Grid,
    Link,
    CssBaseline,
    Button,
    CircularProgress
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { signInUser } from '../service'
import { Footer } from '../../layouts/components/Footer'

// validation
const schema = {
    username: {
        email: true,
        presence: { allowEmpty: false, message: 'Email is required' },
        length: {
            maximum: 64,
        },
    },
    password: {
        presence: { allowEmpty: false, message: 'Password is required' },
        length: {
            maximum: 128,
        },
    },
};

// styles
const useStyles = makeStyles(theme => ({
    '@global': {
        body: {
            backgroundColor: theme.palette.common.white,
        },
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

function Login() {
    const classes = useStyles();
    const dispatch = useDispatch();
    const auth = useSelector(state => state.auth)
    const [formState, setFormState] = useState({
        isValid: false,
        values: {},
        touched: {},
        errors: {},
    })

    useEffect(() => {
        const errors = validate(formState.values, schema);

        setFormState(formState => ({
            ...formState,
            isValid: errors ? false : true,
            errors: errors || {},
        }));
    }, [formState.values]);

    if (auth.isAuthenticated) {
        return <Redirect to="/" />;
    }

    const handleOnSubmit = (event) => {
        event.preventDefault()
        dispatch(signInUser(formState.values))
    }

    const handleChange = (event) => {
        event.persist()

        setFormState(formState => ({
            ...formState,
            values: {
                ...formState.values,
                [event.target.name]: event.target.value,
            },
            touched: {
                ...formState.touched,
                [event.target.name]: true,
            },
        }))
    }

    const hasError = field => formState.touched[field] && formState.errors[field] ? true : false;

    return (
        <React.Fragment>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <div className={classes.paper}>
                    <Typography align="center">
                        <img
                            src={Logo}
                            width="60%"
                            alt="mbj"
                        />
                    </Typography>
                    {auth.signInFailed && auth.error && (
                        <Typography color="error" align="center">
                            {auth.error.error === 'invalid_credentials'
                                ? 'Incorrect username or password.'
                                : auth.error.message}
                        </Typography>
                    )}
                    <div className={classes.form}>
                        <TextField
                            type="text"
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            label="Email Address"
                            error={hasError('username')}
                            helperText={hasError('username') ? formState.errors.username[0] : null}
                            name="username"
                            value={formState.values.username || ''}
                            onChange={handleChange}
                            autoComplete="username"
                            autoFocus
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            error={hasError('password')}
                            helperText={hasError('password') ? formState.errors.password[0] : null}
                            type="password"
                            value={formState.values.password || ''}
                            onChange={handleChange}
                            autoComplete="current-password"
                        />
                        <Button
                            onClick={handleOnSubmit}
                            type="submit"
                            name="submit"
                            disabled={!formState.isValid || auth.isPending}
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                        >
                            {auth.isPending && (
                                <CircularProgress size="1.5rem" className={classes.progress} />
                            )}
                            {!auth.isPending && 'Sign In'}
                        </Button>
                        <Grid container>
                            <Grid item xs></Grid>
                            <Grid item>
                                <Link href="#" variant="body2">
                                    {"Forgot password?"}
                                </Link>
                            </Grid>
                            <Grid item xs></Grid>
                        </Grid>
                    </div>
                </div>
                {Footer}
            </Container>
        </React.Fragment>
    );
}

Login.propTypes = {
    history: PropTypes.object,
};

export default Login
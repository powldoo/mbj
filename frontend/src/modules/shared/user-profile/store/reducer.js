import * as types from './actionTypes';

const initialState = {
    data: {},
    failed: false,
    fetching: false,
    error: {}
}

function reducer(state = initialState, action) {
    const { type, payload } = action;

    switch (type) {
        case types.FETCH_USER_INFORMATION:
            return {
                ...state,
                fetching: true,
            }
        case types.SUCCESS_USER_INFORMATION:
            return {
                ...state,
                fetching: false,
                data: { ...payload }
            }
        case types.FAIL_USER_INFORMATION:
            return {
                ...state,
                fetching: false,
                failed: true,
                error: { payload }
            }
        default:
            return state;
    }
}

export default reducer;
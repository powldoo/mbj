import * as types from './actionTypes'

export function fetchUserInformation() {
    return {
        type: types.FETCH_USER_INFORMATION
    }
}

export function successUserInformation(data) {
    return {
        type: types.SUCCESS_USER_INFORMATION,
        payload: data
    }
}

export function failUserInformation(error) {
    return {
        type: types.FAIL_USER_INFORMATION,
        payload: error
    }
}
import Http from '../../../utilities/Http';
import {
    fetchUserInformation,
    successUserInformation,
    failUserInformation
} from './store/actionCreators'

export function authUserDetails() {
    return function (dispatch) {
        dispatch(fetchUserInformation())

        return Http.get('api/profile/me')
            .then(response => {
                dispatch(successUserInformation(response.data.data))
            })
            .catch(error => {
                dispatch(failUserInformation(error))
            })
    }
}
import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { useSelector } from 'react-redux'
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Avatar, Grid, Paper, CircularProgress } from '@material-ui/core';
import FaceIcon from '@material-ui/icons/Face'

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    green: {
        color: '#fff',
        backgroundColor: '#4CAF50',
        width: 50,
        height: 50,
    },
    paper: {
        height: 150,
        overflow: 'hidden',
        padding: theme.spacing(2)
    },
    collection: {
        overflow: 'hidden',
        padding: theme.spacing(2)
    },
    button: {
        overflow: 'hidden',
        padding: theme.spacing(2, 0)
    },
    progress: {
        margin: theme.spacing(4, 3)
    }
}));

function Dashboard() {
    const classes = useStyles()
    const authUser = useSelector(state => state.authUser.data)

    return (
        <div className={classes.root}>
            <Grid container spacing={2}>
                <Grid item xs={12} sm={4}>
                    <Paper className={classes.paper}>
                        {authUser.fetching && (
                            <CircularProgress className={classes.progress} />
                        )}
                        {!authUser.fetching && (
                            <div>
                                <Grid container wrap="nowrap" spacing={2}>
                                    <Grid item>
                                        <Avatar className={classes.green}>
                                            <FaceIcon />
                                        </Avatar>
                                    </Grid>
                                    <Grid item xs>
                                        <Typography gutterBottom variant="h5" component="h2">
                                            {authUser.name}
                                        </Typography>
                                        <Typography variant="body2" color="textSecondary" component="p">
                                            {authUser.email}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Grid container wrap="nowrap" className={classes.button}>
                                    <Button size="small" color="primary">
                                        My Profile
                                    </Button>
                                </Grid>
                            </div>
                        )}
                    </Paper>
                </Grid>
                <Grid item xs={12} sm={8}>
                    <Paper className={classes.paper}>
                        Test
                    </Paper>
                </Grid>
            </Grid>
            <Grid container spacing={2}>
                <Grid item xs>
                    <Paper className={classes.collection}>
                        Collection Info
                    </Paper>
                </Grid>
            </Grid>
        </div>
    )
}

export default Dashboard
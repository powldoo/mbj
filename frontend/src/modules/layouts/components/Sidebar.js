import React from 'react';
import { ListItem, ListItemIcon, ListItemText, List, Divider, Link } from '@material-ui/core';
import { useSelector } from 'react-redux'
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople';
import PeopleIcon from '@material-ui/icons/People';
import PlaylistAddCheckIcon from '@material-ui/icons/PlaylistAddCheck';
import HomeIcon from '@material-ui/icons/Home';
import SettingsIcon from '@material-ui/icons/Settings';


function Sidebar() {
    const authUser = useSelector(state => state.authUser.data)

    return (
        <div>
            <List>
                <Link href="/" color="inherit" underline="none">
                    <ListItem button>
                        <ListItemIcon>
                            <HomeIcon />
                        </ListItemIcon>
                        <ListItemText primary="Dashboard" />
                    </ListItem>
                </Link>
                {authUser.role && authUser.role.name === 'Super Administrator' && (
                    <Link href="/users" color="inherit" underline="none">
                        <ListItem button>
                            <ListItemIcon>
                                <EmojiPeopleIcon />
                            </ListItemIcon>
                            <ListItemText primary="Admin Users" />
                        </ListItem>
                    </Link>
                )}
                {authUser.role && authUser.role.name !== 'Client' && (
                    <Link href="/clients" color="inherit" underline="none">
                        <ListItem button>
                            <ListItemIcon>
                                <PeopleIcon />
                            </ListItemIcon>
                            <ListItemText primary="Clients" />
                        </ListItem>
                    </Link>
                )}
                <Link href="/collections" color="inherit" underline="none">
                    <ListItem button>
                        <ListItemIcon>
                            <PlaylistAddCheckIcon />
                        </ListItemIcon>
                        <ListItemText primary="Collections" />
                    </ListItem>
                </Link>
            </List>
            <Divider />
            <Link href="/settings" color="inherit" underline="none">
                <List>
                    <ListItem button>
                        <ListItemIcon>
                            <SettingsIcon />
                        </ListItemIcon>
                        <ListItemText primary="Settings" />
                    </ListItem>
                </List>
            </Link>
        </div>
    )
}

export default Sidebar
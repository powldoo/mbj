import { combineReducers } from 'redux';
import auth from '../modules/auth/store/reducer';
import loader from '../modules/loader/store/reducer';
import authUser from '../modules/shared/user-profile/store/reducer'
import adminList from '../modules/users/store/reducer'

const reducers = combineReducers({
    auth,
    loader,
    authUser,
    adminList
});

export default reducers;
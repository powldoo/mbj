import React, { lazy, Suspense, useEffect } from 'react'
import { useSelector } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import { useDispatch } from 'react-redux'
import { authUserDetails } from '../modules/shared/user-profile/service'

import Loader from '../modules/loader/component/Loader';

function Private(props) {
    const { component, ...rest } = props
    const auth = useSelector(state => state.auth)
    const dispatch = useDispatch()

    // user profile
    useEffect(() => {
        dispatch(authUserDetails())
    }, [dispatch])

    if (!auth.isAuthenticated) {
        return (
            <Route
                {...rest}
                render={props => (
                    <Redirect
                        to={{
                            pathname: '/login',
                            state: { from: props.location },
                        }}
                    />
                )}
            />
        )
    }

    /** Admin Layout */
    const Layout = lazy(() => import(`../modules/layouts/Layout`))
    const Component = lazy(() => import(`../${component}`));
    const renderLoader = Loader;

    return (
        <Route
            {...rest}
            render={props => (
                <Suspense fallback={renderLoader()}>
                    <Layout>
                        <Component {...props} />
                    </Layout>
                </Suspense>
            )}
        />
    );
}

export default Private
import React, { lazy, Suspense } from 'react'
import { Route } from 'react-router-dom'

import Loader from '../modules/loader/component/Loader'

function Public(props) {
    const { component, ...rest } = props
    const Component = lazy(() => import(`../${component}`))
    const renderLoader = Loader

    return (
        <Route
            {...rest}
            render={props => (
                <Suspense fallback={renderLoader()}>
                    <Component {...props} />
                </Suspense>
            )}
        />
    )
}

export default Public
export default [
    {
        path: '/',
        component: 'modules/dashboard/pages/Dashboard',
        auth: true,
    },
    {
        path: '/users',
        component: 'modules/users/pages/Users',
        auth: true,
    },
    {
        path: '/clients',
        component: 'modules/clients/pages/Clients',
        auth: true,
    },
    {
        path: '/collections',
        component: 'modules/collections/pages/Collections',
        auth: true,
    },
    {
        path: '/settings',
        component: 'modules/settings/pages/Settings',
        auth: true,
    },
    {
        path: '/login',
        component: 'modules/auth/pages/Login',
    },
]